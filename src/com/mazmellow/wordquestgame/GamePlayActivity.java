package com.mazmellow.wordquestgame;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import com.google.ads.an;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class GamePlayActivity extends Activity {

	QuestionObject questionNow;

	TextView titleQuestion, numcoin;
	Button buttonCoin;
	LinearLayout lineanswer_1, lineanswer_2, lineCharacter1, lineCharacter2, lineCharacter3;
	FrameLayout frameImage;
	DatabaseManager dbManager;
	
	String[] answerBlocks;
	int indexFree;

	int ids[] = { R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1,
			R.drawable.question_1, R.drawable.question_1, R.drawable.question_1 };

	int stage;

	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gameplay);
		dbManager = new DatabaseManager(this);

		titleQuestion = (TextView) findViewById(R.id.title_question);
		frameImage = (FrameLayout) findViewById(R.id.frameImage);
		buttonCoin = (Button) findViewById(R.id.button_coin);
		numcoin = (TextView) findViewById(R.id.numcoin);
		lineanswer_1 = (LinearLayout) findViewById(R.id.lineanswer_1);
		lineanswer_2 = (LinearLayout) findViewById(R.id.lineanswer_2);
		lineCharacter1 = (LinearLayout) findViewById(R.id.lineCharacter1);
		lineCharacter2 = (LinearLayout) findViewById(R.id.lineCharacter2);
		lineCharacter3 = (LinearLayout) findViewById(R.id.lineCharacter3);

		stage = 0;
	}

	void refreshQuestion() {
		questionNow = MainActivity.questions.get(stage);
		titleQuestion.setText("คำถามที่ " + (stage + 1));

		setQuestionImage();
		makeAnswerWord();
		randomCharacterBlock();
	}

	void setQuestionImage(){
		frameImage.removeAllViews();
		final ImageView imageView = new ImageView(this);
		LayoutParams params = (new LayoutParams(
				LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		params.gravity = Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL;
		imageView.setLayoutParams(params);
		
		if(stage<50){
			//get image from resource
			imageView.setImageDrawable(getResources().getDrawable(ids[stage]));
		}else{
			byte[] bs = questionNow.getImage();
			if(bs!=null && bs.length>0){
				Bitmap bmp = BitmapFactory.decodeByteArray(bs,0,bs.length);
				imageView.setImageBitmap(bmp);
			}else{
				//request and save
				new Thread(){
					public void run() {
						try {
							URL url = new URL(questionNow.getUrl());
							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							connection.setDoInput(true);
							connection.connect();
							InputStream input = connection.getInputStream();
							Bitmap myBitmap = BitmapFactory.decodeStream(input);
							imageView.setImageBitmap(myBitmap);
							questionNow.setImage(convertInputStreamToByteArray(input));
							updateQuestionNowToDB();
						} catch (IOException e) {
							e.printStackTrace();
							
						}
					};
				}.start();
			}
		}
		
	}

	void updateQuestionNowToDB() {
		// TODO Auto-generated method stub
		dbManager.insertOrUpdateQuestion(questionNow.getQid(), questionNow.getVersion(), questionNow.getLevel()
				, questionNow.getAnswer(), questionNow.getUrl()
				, questionNow.getHint1(), questionNow.getHint2(), questionNow.getHint3(), questionNow.getImage(), questionNow.getPass());
	}

	public byte[] convertInputStreamToByteArray(InputStream inputStream) {
		byte[] bytes = null;

		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			byte data[] = new byte[1024];
			int count;

			while ((count = inputStream.read(data)) != -1) {
				bos.write(data, 0, count);
			}

			bos.flush();
			bos.close();
			inputStream.close();

			bytes = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return bytes;
	}

	void makeAnswerWord() {
		lineanswer_1.removeAllViews();
		lineanswer_2.removeAllViews();
		
		String answer = questionNow.getAnswer();
		String[] chars = answer.split(",");
		answerBlocks = new String[chars.length];
		for(int i=0; i<answerBlocks.length; i++){
			answerBlocks[i] = "";
		}
		indexFree = 0;
		
		for(int i=0; i<chars.length; i++){
			Button btnChar = new Button(this);
			LayoutParams params = (new LayoutParams(
					LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
			btnChar.setLayoutParams(params);
			btnChar.setTag(i+"");
			btnChar.setBackgroundResource(R.drawable.redgem_mini);
			btnChar.setText("");
			btnChar.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Button btnChar = (Button)v;
					btnChar.setBackgroundResource(R.drawable.redgem_mini);
					btnChar.setText("");
					int index = Integer.parseInt((String)btnChar.getTag());
					answerBlocks[index] = "";
					getFirstIndexFree();
				}
			});
			if(i<8){
				lineanswer_1.addView(btnChar);
			}else{
				lineanswer_2.addView(btnChar);
			}
		}
	}
	
	void getFirstIndexFree(){
		int index = -1;
		for(int i=0;i<answerBlocks.length;i++){
			if(answerBlocks[i].endsWith("")){
				index = i;
				break;
			}
		}
		indexFree = index;
	}

	void randomCharacterBlock() {
		// generate character
		lineCharacter1.removeAllViews();
		lineCharacter2.removeAllViews();
		lineCharacter3.removeAllViews();
		
		String hint3 = questionNow.getHint3();
		String[] chars = hint3.split(",");
		String answer = questionNow.getAnswer();
		String[] chars2 = answer.split(",");
		
		String[] char3 = new String[chars.length + chars2.length];
		System.arraycopy(chars, 0, char3, 0, chars.length);
	    System.arraycopy(chars2, 0, char3, chars.length, chars2.length);
		char3 = shuffleArray(char3);
		
		for(int i=0; i<chars.length; i++){
			Button btnChar = new Button(this);
			LayoutParams params = (new LayoutParams(
					LayoutParams.WRAP_CONTENT,
					LayoutParams.WRAP_CONTENT));
			btnChar.setLayoutParams(params);
			btnChar.setBackgroundResource(R.drawable.bluegem_mini);
			btnChar.setText(char3[i]);
			btnChar.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
				}
			});
			if(i<8){
				lineCharacter1.addView(btnChar);
			}else if(i>=8 && i<16){
				lineCharacter2.addView(btnChar);
			}else{
				lineCharacter3.addView(btnChar);
			}
		}
	}
	
	String[] shuffleArray(String[] ar)
	  {
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--)
	    {
	      int index = rnd.nextInt(i + 1);
	      // Simple swap
	      String a = ar[index];
	      ar[index] = ar[i];
	      ar[i] = a;
	    }
	    
	    return ar;
	  }

	boolean checkAnswer() {

		return true;
	}

}
