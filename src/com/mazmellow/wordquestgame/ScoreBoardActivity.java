package com.mazmellow.wordquestgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mazmello.uil.BaseActivity;
import com.mazmellow.wordquestgame.RequestHttpClient.RequestHttpClientListenner;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ScoreBoardActivity extends BaseActivity implements
		RequestHttpClientListenner {

	public final int USER_TODAY = 0;
	public final int USER_ALL = 1;
	public final int USER_FRIENDS = 2;

	int typeUser = -1;

	DisplayImageOptions options;

	Button btnUserToday, btnUserAll, btnUserFriends, btnFindUser;
	ListView listView;
	ItemAdapter adapter;

	JSONArray userArray;
	RequestHttpClient requestHttpClient;

	
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scoreboard);
		typeUser = USER_TODAY;

		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisc(true)
				.displayer(new FadeInBitmapDisplayer(500)/*
														 * RoundedBitmapDisplayer(
														 * 20)
														 */).build();

		userArray = new JSONArray();

		btnUserToday = (Button) findViewById(R.id.btnUserToday);
		btnUserToday.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requestUser(USER_FRIENDS);
			}
		});

		btnUserAll = (Button) findViewById(R.id.btnUserAll);
		btnUserAll.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requestUser(USER_ALL);
			}
		});

		btnUserFriends = (Button) findViewById(R.id.btnUserFriends);
		btnUserFriends.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				requestUser(USER_FRIENDS);
			}
		});

		btnFindUser = (Button) findViewById(R.id.btnFindUser);
		btnFindUser.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(MainActivity.isLogin){
					//search user by fbid
				}else{
					new AlertDialog.Builder(ScoreBoardActivity.this)
					.setTitle("คุณต้องล็อคอิน Facebook ก่อนถึงจะหาลำดับของคุณได้นะคะ")
					.setMessage("")
					.setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(
										DialogInterface dialog,
										int which) {
								}
							}).show();
				}
			}
		});

		listView = (ListView) findViewById(R.id.listView1);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

			}
		});

	}

	void requestUser(int type) {
		typeUser = type;
		String url = "";
		switch (type) {
		case USER_TODAY:
			url = "";
			break;
		case USER_ALL:
			url = "";
			break;
		case USER_FRIENDS:
			url = "";
			break;
		default:
			break;
		}

		requestHttpClient = new RequestHttpClient(url, this, this);
		requestHttpClient.start();
	}

	
	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub
		try {
			JSONArray jsonArray = new JSONArray(response);

			if (jsonArray != null && jsonArray.length() > 0) {

//				titles = new String[jsonArray.length()];
//				descs = new String[jsonArray.length()];
//				imageUrls = new String[jsonArray.length()];
//				appids = new String[jsonArray.length()];
//
//				for (int i = jsonArray.length() - 1; i >= 0; i--) {
//					JSONObject jsonObj = jsonArray.getJSONObject(i);
//
//					if (jsonObj.has("title"))
//						titles[i] = jsonObj.getString("title");
//					if (jsonObj.has("desc"))
//						descs[i] = jsonObj.getString("desc");
//					if (jsonObj.has("img"))
//						imageUrls[i] = jsonObj.getString("img");
//					if (jsonObj.has("appid"))
//						appids[i] = jsonObj.getString("appid");
//				}
				adapter.notifyDataSetChanged();
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error = " + e.getMessage());
		}
	}

	
	public void onRequestError(String error) {
		// TODO Auto-generated method stub

	}

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public TextView desc;
			public ImageView image;
		}

		
		public int getCount() {
			return userArray.length();
		}

		
		public Object getItem(int position) {
			return position;
		}

		
		public long getItemId(int position) {
			return position;
		}

		
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.item_list_image,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.desc = (TextView) view.findViewById(R.id.desc);
				holder.image = (ImageView) view.findViewById(R.id.image);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			// holder.text.setText(titles[position]);
			// holder.desc.setText(descs[position]);
			// imageLoader.displayImage(imageUrls[position], holder.image,
			// options, animateFirstListener);

			return view;
		}
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}
