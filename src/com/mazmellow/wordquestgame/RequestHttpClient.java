package com.mazmellow.wordquestgame;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class RequestHttpClient{
	
	ProgressDialog progressDialog;
	RequestHttpClientListenner listenner;
	String url;
	Context activity;
	String word_loading;
	
	public interface RequestHttpClientListenner{
		public void onRequestStringCallback(String response);
		public void onRequestError(String error);
	}
	
	public RequestHttpClient(String _url, RequestHttpClientListenner _listenner, Context _activity){
		url = _url;
		listenner = _listenner;
		activity = _activity;
		word_loading = "Please wait...";
	}
	
	public RequestHttpClient(String _url, RequestHttpClientListenner _listenner, Context _activity, String word){
		url = _url;
		listenner = _listenner;
		activity = _activity;
		word_loading = word;
	}
	
	public void start(){
		if(url!=null && !url.equals("")){
			if (activity!=null && progressDialog == null) {
                progressDialog = new ProgressDialog(activity);
                progressDialog.setMessage(word_loading);
                progressDialog.show();
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(true);
            }   
			AsyncHttpClient client = new AsyncHttpClient();
			client.get(url, new AsyncHttpResponseHandler() {
			    
			    public void onSuccess(String response) {
//			        Log.i("RequestHttpClient", response);
			    	try {
			    		if (progressDialog!=null && progressDialog.isShowing()) {
		                    progressDialog.dismiss();
		                }
			    		
			    		if(listenner!=null){
			    			response = response.trim();
				        	listenner.onRequestStringCallback(response);
				        }
				         
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("ERROR = "+e.getMessage());
						if(listenner!=null){
			    			response = response.trim();
				        	listenner.onRequestError(e.getMessage());
				        }
					}
			          
			    }
			});
		}
	}
    
}
