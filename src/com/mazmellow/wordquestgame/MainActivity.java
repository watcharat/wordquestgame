package com.mazmellow.wordquestgame;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.ProfilePictureView;
import com.mazmellow.wordquestgame.RequestHttpClient.RequestHttpClientListenner;

public class MainActivity extends Activity {
	
	public static int stage;
	public static ArrayList<QuestionObject> questions;
	
	public static final String APP_VERSION = "app_version";
	public static final String QUESTION_VERSION = "question_version";
	
	public static int client_AppVersion;
	public static int client_QuestionVersion;
	public static int server_AppVersion;
	public static int server_QuestionVersion;

	private static final String PERMISSION = "publish_actions";

	private final String PENDING_ACTION_BUNDLE_KEY = "com.facebook.samples.hellofacebook:PendingAction";

	private MyFacebookLoginButton loginButton;
	private ProfilePictureView profilePictureView;
	private TextView greeting;
	private PendingAction pendingAction = PendingAction.NONE;
	private GraphUser user;
	private boolean canPresentShareDialog;

	public static boolean isLogin;
	public static String fbid, fb_username, fb_userimage, fb_accessToken;
	
	public static String user_friend, user_friend_request, user_lasted_date;
	public static int user_lasted_score, user_best_score;
	
	public static String currentDate;

	private enum PendingAction {
		NONE, POST_PHOTO, POST_STATUS_UPDATE
	}

	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
		
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		
		public void onError(FacebookDialog.PendingCall pendingCall,
				Exception error, Bundle data) {
			Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
		}

		
		public void onComplete(FacebookDialog.PendingCall pendingCall,
				Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};

	SharedPreferences sharedPref;
	DatabaseManager dbManager;
	
	
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		dbManager = new DatabaseManager(this);
		
		stage = 1;
		
		if (sharedPref == null) {
			sharedPref = this.getSharedPreferences(
					getString(R.string.preference_file_key),
					Context.MODE_PRIVATE);
		}
		
		//check lasted app_version
		client_AppVersion = sharedPref.getInt(APP_VERSION, 0);
		if(client_AppVersion == 0){
			//insert version 1
			client_AppVersion = 1;
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putInt(APP_VERSION, client_AppVersion);
			editor.commit();
		}
		
		//check lasted question_version
		client_QuestionVersion = sharedPref.getInt(QUESTION_VERSION, 0);
		if(client_QuestionVersion == 0){
			//insert version 1
			client_QuestionVersion = 1;
			
			addFirstQuestion();
			
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putInt(QUESTION_VERSION, client_QuestionVersion);
			editor.commit();
		}
		
		questions = dbManager.getAllQuestion();
		
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			String name = savedInstanceState
					.getString(PENDING_ACTION_BUNDLE_KEY);
			pendingAction = PendingAction.valueOf(name);
		}

		loginButton = (MyFacebookLoginButton) findViewById(R.id.login_button);
		loginButton
				.setUserInfoChangedCallback(new MyFacebookLoginButton.UserInfoChangedCallback() {
					
					public void onUserInfoFetched(GraphUser user) {
						MainActivity.this.user = user;
						updateUI();
						
						if(fbid!=null){
							requestUserImage();
						}
						
						// It's possible that we were waiting for this.user to
						// be populated in order to post a
						// status update.
						handlePendingAction();
					}
				});

		profilePictureView = (ProfilePictureView) findViewById(R.id.profilePicture);
		greeting = (TextView) findViewById(R.id.greeting);

		canPresentShareDialog = FacebookDialog.canPresentShareDialog(this,
				FacebookDialog.ShareDialogFeature.SHARE_DIALOG);

		Button btnStartGame = (Button) findViewById(R.id.btnStartGame);
		btnStartGame.setOnClickListener(new OnClickListener() {

			
			public void onClick(View v) {
				// TODO check facebook login
				if (isLogin) {
					// open start game
					Intent intent = new Intent(MainActivity.this, GamePlayActivity.class);
					startActivity(intent);
				} else {
					// please login before using
					new AlertDialog.Builder(MainActivity.this)
							.setTitle("กรุณาล็อคอิน Facebook ก่อนใช้งานนะคะ")
							.setMessage("")
							.setPositiveButton("ตกลง",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
										}
									}).show();
				}
			}
		});

		Button btnScoreBoard = (Button) findViewById(R.id.btnScoreBoard);
		btnScoreBoard.setOnClickListener(new OnClickListener() {

			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, ScoreBoardActivity.class);
				startActivity(intent);
			}
		});
		
		checkVersion();
	}	

	void checkVersion(){
		RequestHttpClient httpClient = new RequestHttpClient("http://www.mazmellow.com/gameword/version.php", new RequestHttpClientListenner() {
			
			
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				try {
					JSONArray jsonArray = new JSONArray(response);
					JSONObject jsonObj = jsonArray.getJSONObject(0);

					if (jsonObj.has("app_version"))
						server_AppVersion = jsonObj.getInt("app_version");
					if (jsonObj.has("question_version"))
						server_QuestionVersion = jsonObj.getInt("question_version");
					
					if(server_AppVersion > client_AppVersion){
						//request update app
					}else{
						if(server_QuestionVersion > client_QuestionVersion){
							//request update question
							requestUpdateQuestion();
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("error = " + e.getMessage());
				}
			}
			
			
			public void onRequestError(String error) {
				// TODO Auto-generated method stub
				
			}
		}, MainActivity.this);
		httpClient.start();
	}
	
	void requestUpdateQuestion(){
		String url = "http://www.mazmellow.com/gameword/question.php?version="+client_QuestionVersion;
		RequestHttpClient httpClient = new RequestHttpClient(url, new RequestHttpClientListenner() {
			
			
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				try {
					JSONArray jsonArray = new JSONArray(response);
					for(int i=0; i<jsonArray.length(); i++){
						JSONObject jsonObj = jsonArray.getJSONObject(i);
						//insert to database
						dbManager.insertOrUpdateQuestion(jsonObj.getString("qid"), jsonObj.getInt("version"), jsonObj.getInt("level")
								, jsonObj.getString("answer"), jsonObj.getString("url"), jsonObj.getString("hint1")
								, jsonObj.getString("hint2"), jsonObj.getString("hint3"), null, "n");
					}
					
					questions = dbManager.getAllQuestion();
					
					//toast success
					Toast toast = Toast.makeText ( MainActivity.this, "อัพเดทข้อมูลเรียบร้อยแล้ว", Toast.LENGTH_LONG );
					toast.show();
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("error = " + e.getMessage());
				}
			}
			
			
			public void onRequestError(String error) {
				// TODO Auto-generated method stub
				
			}
		}, MainActivity.this, "กำลังอัพเดทข้อมูล");
		httpClient.start();
	}
	
	void requestUserImage(){
		
		//test get picture url
		String imgUserUrl = "https://graph.facebook.com/"+fbid+"?fields=picture.width(160).height(160)&access_token="+fb_accessToken;
		RequestHttpClient requestHttpClient = new RequestHttpClient(imgUserUrl, new RequestHttpClientListenner() {
			
			
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				try {
					JSONObject jsonObject = new JSONObject(response);
					if(jsonObject.has("picture")){
						JSONObject picJsonObject = jsonObject.getJSONObject("picture");
						if(picJsonObject.has("data")){
							JSONObject dataJsonObject = picJsonObject.getJSONObject("data");
							if(dataJsonObject.has("url")){
								fb_userimage = dataJsonObject.getString("url");
								System.out.println("fb_userimage = "+fb_userimage);
								
								requestLogin();
							}
						}
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			public void onRequestError(String error) {
				// TODO Auto-generated method stub
				
			}
		}, null);
		requestHttpClient.start();
	}
	
	 void requestLogin(){
		RequestHttpClient httpClient = new RequestHttpClient("http://www.mazmellow.com/gameword/login.php?fbid="+fbid, new RequestHttpClientListenner() {
			
			
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				try {
					JSONArray jsonArray = new JSONArray(response);
					if(jsonArray.length()>0){
						//login success
						JSONObject jsonObj = jsonArray.getJSONObject(0);
						if (jsonObj.has("friend"))
							user_friend = jsonObj.getString("friend");
						if (jsonObj.has("friend_request"))
							user_friend_request = jsonObj.getString("friend_request");
						if (jsonObj.has("lasted_score"))
							user_lasted_score = jsonObj.getInt("lasted_score");
						if (jsonObj.has("best_score"))
							user_best_score = jsonObj.getInt("best_score");
						if (jsonObj.has("lasted_date"))
							user_lasted_date = jsonObj.getString("lasted_date");
						
						//update user
						requestUpdateUser();
						
					}else{
						//no user >> create new
						
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						currentDate = sdf.format(new Date());
						
						String name = "";
						try {
							name = URLEncoder.encode(fb_username, "utf-8");
						} catch (UnsupportedEncodingException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						String url = "http://www.mazmellow.com/gameword/createuser.php?fbid="+fbid+"&name="+name+"&image"+"&image="+fb_userimage+"&date="+currentDate;
						RequestHttpClient httpClient2 = new RequestHttpClient(url, new RequestHttpClientListenner() {
							
							
							public void onRequestStringCallback(String response) {
								// TODO Auto-generated method stub
								try {
									JSONArray jsonArray = new JSONArray(response);
									JSONObject jsonObj = jsonArray.getJSONObject(0);
									if (jsonObj.has("friend"))
										user_friend = jsonObj.getString("friend");
									if (jsonObj.has("friend_request"))
										user_friend_request = jsonObj.getString("friend_request");
									if (jsonObj.has("lasted_score"))
										user_lasted_score = jsonObj.getInt("lasted_score");
									if (jsonObj.has("best_score"))
										user_best_score = jsonObj.getInt("best_score");
									if (jsonObj.has("lasted_date"))
										user_lasted_date = jsonObj.getString("lasted_date");
									
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									System.out.println("error = " + e.getMessage());
								}
							}
							
							
							public void onRequestError(String error) {
								// TODO Auto-generated method stub
								
							}
						}, null);
						httpClient2.start();
					}
					
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("error = " + e.getMessage());
				}
			}
			
			
			public void onRequestError(String error) {
				// TODO Auto-generated method stub
				
			}
		}, null);
		httpClient.start();
	}
	
	void requestUpdateUser(){
		String name = "";
		try {
			name = URLEncoder.encode(fb_username, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String url = "http://www.mazmellow.com/gameword/updateuser.php?fbid="+fbid+"&name="+name
				+"&image"+"&image="+fb_userimage+"&friend="+user_friend+"&friend_request="+user_friend_request
				+"&best_score="+user_best_score+"&lasted_score="+user_lasted_score+"&lasted_date="+user_lasted_date;
		RequestHttpClient httpClient = new RequestHttpClient(url, new RequestHttpClientListenner() {
			
			
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				
			}
			
			
			public void onRequestError(String error) {
				// TODO Auto-generated method stub
				
			}
		}, null);
		httpClient.start();
	}
	
	public void loginFacebook(){
		Session currentSession = Session.getActiveSession();
        if (currentSession == null || currentSession.getState().isClosed()) {
        	String appId = getResources().getString(R.string.app_id);
            Session session = new Session.Builder(this).setApplicationId(appId).build();
            Session.setActiveSession(session);
            currentSession = session;
        }
        if (!currentSession.isOpened()) {
            Session.OpenRequest openRequest = new Session.OpenRequest(this);

            if (openRequest != null) {
            	openRequest.setDefaultAudience(SessionDefaultAudience.EVERYONE);
                openRequest.setLoginBehavior(SessionLoginBehavior.SSO_WITH_FALLBACK);
                currentSession.openForPublish(openRequest);
            	
//                openRequest.setDefaultAudience(properties.defaultAudience);
//                openRequest.setPermissions(properties.permissions);
//                openRequest.setLoginBehavior(properties.loginBehavior);
//
//                if (SessionAuthorizationType.PUBLISH.equals(properties.authorizationType)) {
//                    currentSession.openForPublish(openRequest);
//                } else {
//                    currentSession.openForRead(openRequest);
//                }
            }
        }
	}

	
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();

		// Call the 'activateApp' method to log an app event for use in
		// analytics and advertising reporting. Do so in
		// the onResume methods of the primary Activities that an app may be
		// launched into.
		AppEventsLogger.activateApp(this);

		updateUI();
		
		if(fbid!=null){
			requestUserImage();
		}
	}

	
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);

		outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
	}

	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (pendingAction != PendingAction.NONE
				&& (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
			new AlertDialog.Builder(MainActivity.this)
					.setTitle(R.string.cancelled)
					.setMessage(R.string.permission_not_granted)
					.setPositiveButton(R.string.ok, null).show();
			pendingAction = PendingAction.NONE;
		} else if (state == SessionState.OPENED_TOKEN_UPDATED) {
			handlePendingAction();
		}
		updateUI();
		
	}

	private void updateUI() {
		Session session = Session.getActiveSession();
		boolean enableButtons = (session != null && session.isOpened());

		if (enableButtons && user != null) {
			profilePictureView.setProfileId(user.getId());
			greeting.setText(getString(R.string.hello_user, user.getFirstName()));
			isLogin = true;
			
			fbid = user.getId();
			fb_username = user.getName();
			fb_accessToken = session.getAccessToken();

		} else {
			profilePictureView.setProfileId(null);
			greeting.setText(null);
			isLogin = false;
			
			fbid = null;
			fb_username = "";
			fb_accessToken = null;
		}
	}

	@SuppressWarnings("incomplete-switch")
	private void handlePendingAction() {
		PendingAction previouslyPendingAction = pendingAction;
		// These actions may re-set pendingAction if they are still pending, but
		// we assume they
		// will succeed.
		pendingAction = PendingAction.NONE;

		switch (previouslyPendingAction) {
		case POST_PHOTO:

			break;
		case POST_STATUS_UPDATE:
			postStatusUpdate();
			break;
		}
	}

	private interface GraphObjectWithId extends GraphObject {
		String getId();
	}

	private void showPublishResult(String message, GraphObject result,
			FacebookRequestError error) {
		String title = null;
		String alertMessage = null;
		if (error == null) {
			title = getString(R.string.success);
			String id = result.cast(GraphObjectWithId.class).getId();
			alertMessage = getString(R.string.successfully_posted_post,
					message, id);
		} else {
			title = getString(R.string.error);
			alertMessage = error.getErrorMessage();
		}

		new AlertDialog.Builder(this).setTitle(title).setMessage(alertMessage)
				.setPositiveButton(R.string.ok, null).show();
	}

	private void onClickPostStatusUpdate() {
		performPublish(PendingAction.POST_STATUS_UPDATE, canPresentShareDialog);
	}

	private FacebookDialog.ShareDialogBuilder createShareDialogBuilder() {
		return new FacebookDialog.ShareDialogBuilder(this)
				.setName("Hello Facebook")
				.setDescription(
						"The 'Hello Facebook' sample application showcases simple Facebook integration")
				.setLink("http://developers.facebook.com/android");
	}

	private void postStatusUpdate() {
		if (canPresentShareDialog) {
			FacebookDialog shareDialog = createShareDialogBuilder().build();
			uiHelper.trackPendingDialogCall(shareDialog.present());
		} else if (user != null && hasPublishPermission()) {
			final String message = getString(R.string.status_update,
					user.getFirstName(), (new Date().toString()));
			Request request = Request.newStatusUpdateRequest(
					Session.getActiveSession(), message, null, null,
					new Request.Callback() {
						
						public void onCompleted(Response response) {
							showPublishResult(message,
									response.getGraphObject(),
									response.getError());
						}
					});
			request.executeAsync();
		} else {
			pendingAction = PendingAction.POST_STATUS_UPDATE;
		}
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this).setTitle(title).setMessage(message)
				.setPositiveButton(R.string.ok, null).show();
	}

	private boolean hasPublishPermission() {
		Session session = Session.getActiveSession();
		return session != null
				&& session.getPermissions().contains("publish_actions");
	}

	private void performPublish(PendingAction action, boolean allowNoSession) {
		Session session = Session.getActiveSession();
		if (session != null) {
			pendingAction = action;
			if (hasPublishPermission()) {
				// We can do the action right away.
				handlePendingAction();
				return;
			} else if (session.isOpened()) {
				// We need to get new permissions, then complete the action when
				// we get called back.
				session.requestNewPublishPermissions(new Session.NewPermissionsRequest(
						this, PERMISSION));
				return;
			}
		}

		if (allowNoSession) {
			pendingAction = action;
			handlePendingAction();
		}
	}
	
	private void addFirstQuestion() {
		// TODO add question
		dbManager.insertOrUpdateQuestion("0001", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0002", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0003", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0004", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0005", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0006", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0007", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0008", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0009", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0010", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0011", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0012", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0013", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0014", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0015", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0016", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0017", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0018", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0019", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0020", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0021", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0022", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0023", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0024", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0025", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0026", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0027", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0028", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0029", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0030", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0031", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0032", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0033", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0034", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0035", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0036", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0037", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0038", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0039", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0040", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0041", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0042", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0043", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0044", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0045", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
		dbManager.insertOrUpdateQuestion("0046", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0047", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0048", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0049", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		dbManager.insertOrUpdateQuestion("0050", 1, 1, "answer", "url", "hint1", "hint2", "hint3", null, "n");
		
	}
}
