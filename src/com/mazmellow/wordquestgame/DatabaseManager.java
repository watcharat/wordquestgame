package com.mazmellow.wordquestgame;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

	Context context;

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "wordquestgame.db";

	// Table Name
	private static final String TABLE_QUESTION = "question";

	// -------VERSION 1-------------
	// ---bookmarks---
	
	private static final String COL_QID = "qid";
	private static final String COL_VERSION = "version";
	private static final String COL_LEVEL = "level";
	private static final String COL_ANSWER = "answer";
	private static final String COL_URL = "url";
	private static final String COL_HINT1 = "hint1";
	private static final String COL_HINT2 = "hint2";
	private static final String COL_HINT3 = "hint3";
	private static final String COL_IMAGE = "image";
	private static final String COL_PASS = "pass";
	// -------------------------------

	public DatabaseManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;

	}

	
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		System.out.println("------ onCreate --------");
		try {
			db.execSQL("CREATE TABLE " + TABLE_QUESTION
					+ "("
					+ " " + COL_QID + " TEXT," 
					+ " " + COL_VERSION + " INT,"
					+ " " + COL_LEVEL + " INT," 
					+ " " + COL_ANSWER + " TEXT,"
					+ " " + COL_URL + " TEXT,"
					+ " " + COL_HINT1 + " TEXT,"
					+ " " + COL_HINT2 + " TEXT,"
					+ " " + COL_HINT3 + " TEXT" 
					+ " " + COL_IMAGE + " BLOB"
					+ " " + COL_PASS + " TEXT"
					+");");

			Log.d("CREATE TABLE", "Create Table Successfully.");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("DB ERROR: " + e);
		}

	}

	
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		System.out.println("------ onOpen --------");
	}

	
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		System.out.println("------ onUpgrade --------");
		System.out.println("------ oldVersion : " + oldVersion);
		System.out.println("------ newVersion : " + newVersion);

		if (oldVersion == 1 && newVersion == 2) {
//			createTableRead(db);
		}
	}

	// Insert Data
	public boolean insertOrUpdateQuestion(String qid, int version, int level, String answer, String url,
			String hint1, String hint2, String hint3, byte[] image, String pass) {
		// TODO Auto-generated method stub
		
		if (getQuestionBy(qid) == null) {
			try {
				SQLiteDatabase db = this.getWritableDatabase();
				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL
				 * = "INSERT INTO " + TABLE_MEMBER +
				 * "(MemberID,Name,Tel) VALUES (?,?,?)";
				 * 
				 * insertCmd = db.compileStatement(strSQL);
				 * insertCmd.bindString(1, strMemberID); insertCmd.bindString(2,
				 * strName); insertCmd.bindString(3, strTel); return
				 * insertCmd.executeInsert();
				 */

				ContentValues Val = new ContentValues();
				Val.put(COL_QID, qid);
				Val.put(COL_VERSION, version);
				Val.put(COL_LEVEL, level);
				Val.put(COL_ANSWER, answer);
				Val.put(COL_URL, url);
				Val.put(COL_HINT1, hint1);
				Val.put(COL_HINT2, hint2);
				Val.put(COL_HINT3, hint3);
				Val.put(COL_IMAGE, image);
				Val.put(COL_PASS, pass);

				db.insert(TABLE_QUESTION, null, Val);

				if (db != null) {
					db.close();
				}

				return true; // return rows inserted.

			} catch (Exception e) {
				Log.i("insertOrUpdateMovie", e.getMessage());
				return false;
			}
		} else {
			return true;
		}

	}

	public QuestionObject getQuestionBy(String qid) {
		// TODO Auto-generated method stub
		QuestionObject questionObject = new QuestionObject();
		try {
			// SQLiteDatabase db;
			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_QUESTION + " WHERE "
					+ COL_QID + " = '" + qid + "'";
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					questionObject.setQid(cursor.getString(0));
					questionObject.setVersion(cursor.getInt(1));
					questionObject.setLevel(cursor.getInt(2));
					questionObject.setAnswer(cursor.getString(3));
					questionObject.setUrl(cursor.getString(4));
					questionObject.setHint1(cursor.getString(5));
					questionObject.setHint2(cursor.getString(6));
					questionObject.setHint3(cursor.getString(7));
					questionObject.setImage(cursor.getBlob(8));
					questionObject.setPass(cursor.getString(9));
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return questionObject;

		} catch (Exception e) {
			Log.i("getQuestionBy", e.getMessage());
			return null;
		}

	}
	
	public ArrayList<QuestionObject> getAllQuestion() {
		// TODO Auto-generated method stub

		try {
			ArrayList<QuestionObject> list = new ArrayList<QuestionObject>();

			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_QUESTION;
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						QuestionObject questionObject = new QuestionObject();
						questionObject.setQid(cursor.getString(0));
						questionObject.setVersion(cursor.getInt(1));
						questionObject.setLevel(cursor.getInt(2));
						questionObject.setAnswer(cursor.getString(3));
						questionObject.setUrl(cursor.getString(4));
						questionObject.setHint1(cursor.getString(5));
						questionObject.setHint2(cursor.getString(6));
						questionObject.setHint3(cursor.getString(7));
						questionObject.setImage(cursor.getBlob(8));
						questionObject.setPass(cursor.getString(9));
						list.add(questionObject);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return list;

		} catch (Exception e) {
			Log.i("getAllQuestion", e.getMessage());
			// System.out.println("ERROR getAllBookmark: "+e);
			return null;
		}

	}

	// Delete Data
	public boolean deleteQuestionByQid(String qid) {
		// TODO Auto-generated method stub

		try {

			SQLiteDatabase db;
			db = this.getWritableDatabase(); // Write Data

			/**
			 * for API 11 and above SQLiteStatement insertCmd; String strSQL =
			 * "DELETE FROM " + TABLE_MEMBER + " WHERE MemberID = ? ";
			 * 
			 * insertCmd = db.compileStatement(strSQL); insertCmd.bindString(1,
			 * strMemberID);
			 * 
			 * return insertCmd.executeUpdateDelete();
			 * 
			 */

			db.delete(TABLE_QUESTION, COL_QID + " = ?",
					new String[] { String.valueOf(qid) });

			db.close();
			return true; // return rows delete.

		} catch (Exception e) {
			return false;
		}

	}
	
}
